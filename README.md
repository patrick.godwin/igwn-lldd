# igwn-lldd

This provides a containerized version of the igwn-lldd software stack.

A calendar versioning ([CalVer](http://calver.org/)) scheme is used, via the
form `YYYY.MM.MICRO`, e.g. `2022.10.0`.

## Usage

```
docker run -it containers.ligo.org/patrick.godwin/igwn-lldd:latest <cmd> <opts>
```

## Updating the Conda lock file

```
conda-lock lock --file environment.yml -p linux-64 --kind explicit
```
