FROM mambaorg/micromamba:1.4.2
COPY --chown=$MAMBA_USER:$MAMBA_USER conda-linux-64.lock /tmp/conda-linux-64.lock
RUN micromamba install -y -n base -f conda-linux-64.lock && \
    micromamba clean --all --yes
